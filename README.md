# React User Form

## Comments to the solution and assumptions I made

- I assumed only some fields are required in user form (firstName, lastName, email and phone)
- I assumed very simple model for user data as there is no API
- I assumed when entering new route we ask endpoint for current data (no Redux, localstorage etc) -> hence the mocked loader during each route enter
- for Ui components I've chosen Material UI as it's one of few UI libraries I am the most familiar with
- for forms and validation of fields I've chosen Formik and Yup as I find them the simplest
- I used modified BEM-like convention for SCSS classes for convenient usage with modules: .camelCaseName_modifier
- all code is linted without warnings based on rules provided in config files
- there are two different responsive views: mobile/tablet and desktop
- project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Further steps that I would do in this project / improvements:

- inputmask for phone number
- date formatting for birthday
- more tests (currently only one example test file is provided for validation-schema)
- changing strings used in templates to i18n translations
- adding more technical info to Readme
- add requests error handling after proper API service provided
- 404 page

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run lint`

Lint typescript and scss code.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

