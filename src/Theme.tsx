import {createTheme} from '@material-ui/core/styles';
import styles from './Theme.module.scss';

export default createTheme({
  spacing: 10,
  breakpoints: {
    values: {
      xs: 0,
      sm: Number(styles.breakpointSm),
      md: Number(styles.breakpointMd),
      lg: Number(styles.breakpointLg),
      xl: Number(styles.breakpointXl)
    }
  },
  palette: {
    primary: {
      contrastText: styles.white,
      main: styles.primaryColor
    },
    secondary: {
      main: styles.errorColor
    }
  },
  typography: {
    htmlFontSize: Number(styles.baseFontSize),
    fontSize: Number(styles.baseFontSize) * 14 / 16,
    fontFamily: styles.mainFontFamily,
    button: {
      fontSize: styles.fontSizeM,
      textTransform: 'none'
    }
  }
});