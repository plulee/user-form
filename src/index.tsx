import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {StylesProvider, ThemeProvider} from '@material-ui/core';
import {App} from './components/app/App';
import Theme from './Theme';
import './styles/styles.scss';

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={Theme}>
      <StylesProvider injectFirst>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </StylesProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

