import React, {ReactElement} from 'react';
import styles from './Footer.module.scss';

export const Footer = (): ReactElement => (
  <footer className={styles.footer}>
    <div className={styles.contentWrapper}>
      Lorem ipsum dolor sit amet
    </div>
  </footer>
);
