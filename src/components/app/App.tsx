import React, {ReactElement} from 'react';
import {Footer} from '../footer/Footer';
import {Header} from '../header/Header';
import {Menu} from '../menu/Menu';
import {Router} from '../router/Router';
import styles from './App.module.scss';

export const App = (): ReactElement => (
  <div className={styles.appContainer}>
    <Header />
    <div className={styles.mainContentWrapper}>
      <Menu />
      <main className={styles.main}>
        <Router />
      </main>
    </div>
    <Footer />
  </div>
);
