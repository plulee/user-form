import {Formik} from 'formik';
import React, {ReactElement} from 'react';
import {useHistory} from 'react-router-dom';
import MockedHttpService from '../../shared/mocked-http.service';
import useGetUserDataHook from '../../shared/hooks/use-get-user-data.hook';
import {UserData} from '../../shared/model/user-data.model';
import {RoutePaths} from '../router/route-paths';
import {Loader} from '../shared/loader/Loader';
import {UserForm} from './user-form/UserForm';
import {validationSchema} from './validation-schema';

export const UserFormView = (): ReactElement => {

  const history = useHistory();
  const {isUserDataLoading, userData} = useGetUserDataHook();

  const submitForm  = async (values: UserData): Promise<void> =>
    MockedHttpService.postUserData(values)
      .then((): void => history.push(RoutePaths.USER_PROFILE));

  return (
    <>
      <h1>User form</h1>
      {
        isUserDataLoading
          ? <Loader />
          : <Formik<UserData>
              onSubmit={submitForm}
              initialValues={userData}
              validateOnChange={false}
              validationSchema={validationSchema}>
              {
                (props): ReactElement => <UserForm {...props} />
              }
            </Formik>
      }
    </>
  );
};
