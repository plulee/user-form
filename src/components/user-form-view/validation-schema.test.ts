import {validationSchema} from './validation-schema';

const emptyUserForm = ({
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
  birthday: '',
  about: '',
  avatar: ''
});

let result;

describe('validation-schema', () => {

  it('should validate user form as invalid when no required fields are provided', async () => {

    result = await validationSchema.isValid(emptyUserForm);

    expect(result).toEqual(false);
  });

  it('should validate user form as valid when firstName, lastName, email and phone are provided', async () => {

    result = await validationSchema.isValid({
      ...emptyUserForm,
      firstName: 'John',
      lastName: 'Test',
      email: 'test@gmail.com',
      phone: '123456789'
    });

    expect(result).toEqual(true);
  });
});
