import {TextFieldProps} from '@material-ui/core/TextField/TextField';
import {Form, FormikProps} from 'formik';
import React, {ReactElement} from 'react';
import {UserData} from '../../../shared/model/user-data.model';
import {FileUpload} from './file-upload/FileUpload';
import {UserFormInput, UserFormInputProps} from './user-form-input/UserFormInput';
import {SubmitButton} from './submit-button/SubmitButton';
import styles from './UserForm.module.scss';

const dateInputProps = {type: 'date', InputLabelProps: { shrink: true }};

export const UserForm = (
  {values, handleChange, handleBlur, touched, errors, isSubmitting}: FormikProps<UserData>): ReactElement => {

  const getProps = (name: keyof UserData, label: string, additionalProps?: Partial<TextFieldProps>): UserFormInputProps => (
    {
      errorMessage: touched[name] ? errors[name] : undefined,
      textFieldProps: {
        name,
        label,
        value: values[name],
        onBlur: handleBlur,
        onChange: handleChange,
        error: touched[name] && Boolean(errors[name]),
        ...additionalProps
      }
    }
  );

  return (
    <Form className={styles.form}>
      <div className={styles.column}>
        <UserFormInput {...getProps('firstName', 'First name')} />
        <UserFormInput {...getProps('lastName', 'Last name')} />
        <UserFormInput {...getProps('email', 'Email', {type: 'email'})} />
        <UserFormInput {...getProps('phone', 'Phone', {type: 'phone'})} />
        <UserFormInput {...getProps('birthday','Birthday', dateInputProps)} />
        <UserFormInput {...getProps('about', 'About', {multiline: true, maxRows: 4})} />
      </div>
      <div className={styles.column}>
        <FileUpload  />
      </div>
      <div className={styles.buttonWrapper}>
        <SubmitButton disabled={isSubmitting} />
      </div>
    </Form>
  );
}
