import {Button, IconButton} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import {useField} from 'formik';
import React, {ChangeEvent, ReactElement, useCallback} from 'react';
import {FieldError} from '../field-error/FieldError';
import styles from './FileUpload.module.scss';

const ACCEPTED_FILE_FORMATS = 'image/png, image/jpeg';
const MAXIMUM_FILE_SIZE = 1048576; // 1MB

export const FileUpload = (): ReactElement => {

  const [field, meta, helpers] = useField('avatar');
  const {setError, setValue} = helpers;

  const handleFileChange = useCallback((e: ChangeEvent<HTMLInputElement>): void => {

    const file = e.target.files?.[0] ?? null;

    if (!file) {
      setError('Invalid file input')
      return;
    }

    if (file.size > MAXIMUM_FILE_SIZE) {

      // Change bytes to MB / KB
      const maximumSize = MAXIMUM_FILE_SIZE >= 1000000
        ? `${MAXIMUM_FILE_SIZE / 1024 / 1024} MB`
        : `${MAXIMUM_FILE_SIZE / 1024} KB`;

      setError(`File exceeds maximum file size: ${maximumSize}`)
      return;
    }

    e.target.value = ''; // to ensure the same file could be chosen second time after deletion
    setValue(URL.createObjectURL(file));
    setError(undefined);
  }, [setValue, setError]);

  const deleteFile = useCallback((): void => {
    setValue(null);
    setError(undefined);
  }, [setValue, setError]);

  return (
    <>
      <Button color={meta.error ? 'secondary' : 'primary'} variant='outlined' component='label' fullWidth>
        Upload avatar file
        <input type='file' hidden accept={ACCEPTED_FILE_FORMATS} onChange={handleFileChange} />
      </Button>
      <FieldError error={meta.error} />
      {
        !!field.value && (
          <div className={styles.fileWrapper}>
            <img src={field.value} className={styles.thumb} alt='Avatar' />
            <IconButton aria-label='delete' onClick={deleteFile} classes={{root: styles.iconButton}}>
              <DeleteIcon />
            </IconButton>
          </div>
        )
      }
    </>
  );
};
