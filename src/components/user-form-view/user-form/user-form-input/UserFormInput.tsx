import {TextField, TextFieldProps} from '@material-ui/core';
import React, {ReactElement} from 'react';
import {FieldError} from '../field-error/FieldError';

export interface UserFormInputProps {
  textFieldProps: TextFieldProps;
  errorMessage?: string;
}

export const UserFormInput = ({textFieldProps, errorMessage} : UserFormInputProps): ReactElement => (
  <>
    <TextField fullWidth variant='outlined' size='small' {...textFieldProps} />
    <FieldError error={errorMessage} />
  </>
);
