import React, {ReactElement} from 'react';
import styles from './FieldError.module.scss';

interface Props {
  error?: string;
}

export const FieldError = ({error} : Props): ReactElement => (
  <div className={styles.errorWrapper}>
    {error}
  </div>
);
