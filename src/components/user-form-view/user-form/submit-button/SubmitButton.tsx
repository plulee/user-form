import {Button} from '@material-ui/core';
import React, {ReactElement} from 'react';
import styles from './SubmitButton.module.scss';

interface Props {
  disabled: boolean;
}

export const SubmitButton = ({disabled} : Props): ReactElement => (
  <Button color='primary' disabled={disabled} variant='contained' fullWidth type='submit' className={styles.button}>
    Submit
  </Button>
);
