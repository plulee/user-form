import {object, string} from 'yup';

const REQUIRED_ERROR = 'This field is required.'

export const validationSchema = object({
  firstName: string().required(REQUIRED_ERROR),
  lastName: string().required(REQUIRED_ERROR),
  email: string().required(REQUIRED_ERROR),
  phone: string().required(REQUIRED_ERROR),
  birthday: string(),
  about: string(),
  avatar: string()
});
