import React, {ReactElement} from 'react';
import useGetUserDataHook from '../../shared/hooks/use-get-user-data.hook';
import {Loader} from '../shared/loader/Loader';
import {UserDetail} from './user-detail/UserDetail';
import styles from './UserProfileView.module.scss';

export const UserProfileView = (): ReactElement => {

  const {isUserDataLoading, userData} = useGetUserDataHook();
  const {firstName, lastName, email, phone, birthday, about, avatar} = userData;

  return (
    <>
      <h1>User profile</h1>
      {
        isUserDataLoading ? <Loader /> :
          <div className={styles.profileWrapper}>
            <section className={styles.details}>
              <UserDetail label='First name' value={firstName} />
              <UserDetail label='Last name' value={lastName} />
              <UserDetail label='Email' value={email && <a href={`mailto:${email}`}>{email}</a>} />
              <UserDetail label='Phone' value={phone} />
              <UserDetail label='Birthday' value={birthday} />
              <UserDetail label='About' value={about} />
            </section>
            <section className={styles.avatarWrapper}>
              {
                !avatar
                  ? <UserDetail label='Avatar' />
                  : <img src={avatar} className={styles.avatar} alt='Avatar' />

              }
            </section>
          </div>
      }
    </>
  );
};
