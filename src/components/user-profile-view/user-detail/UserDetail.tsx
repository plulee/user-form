import React, {ReactElement, ReactNode} from 'react';
import styles from './UserDetail.module.scss';

interface Props {
  label: string;
  value?: ReactNode;
}

export const UserDetail = ({label, value} : Props): ReactElement => (
  <div className={styles.detail}>
    <span className={styles.label}>
      {label}
    </span>
    {!value ? '-' : value}
  </div>
);
