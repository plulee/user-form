export enum RoutePaths {
  ROOT = '/',
  USER_FORM = '/user-form',
  USER_PROFILE = '/user-profile'
}
