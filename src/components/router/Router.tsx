import React, {ReactElement} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import {UserFormView} from '../user-form-view/UserFormView';
import {UserProfileView} from '../user-profile-view/UserProfileView';
import {RoutePaths} from './route-paths';

export const Router = (): ReactElement => (
  <Switch>
    <Route exact path={RoutePaths.ROOT} >
      <Redirect to={RoutePaths.USER_FORM} />
    </Route>
    <Route exact path={RoutePaths.USER_FORM} component={UserFormView} />
    <Route exact path={RoutePaths.USER_PROFILE} component={UserProfileView} />
  </Switch>
);
