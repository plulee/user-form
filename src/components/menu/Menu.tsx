import {Tab, Tabs, useMediaQuery, useTheme} from '@material-ui/core';
import React, {ReactElement} from 'react';
import {useHistory, useLocation } from 'react-router-dom';
import {RoutePaths} from '../router/route-paths';
import styles from './Menu.module.scss';

const tabClasses = {
  root: styles.tab,
  selected: styles.tab_selected,
  wrapper: styles.label
};


const tabs = [
  {
    key: 'USER_FORM',
    label: 'User form',
    path: RoutePaths.USER_FORM
  },
  {
    key: 'USER_PROFILE',
    label: 'User profile',
    path: RoutePaths.USER_PROFILE
  }
];

export const Menu = (): ReactElement => {

  const currentPath = useLocation().pathname;
  const history = useHistory();
  const theme = useTheme();
  const isDesktop  = useMediaQuery(theme.breakpoints.up('md'));
  const tabsOrientation = isDesktop ? 'vertical' : 'horizontal';

  return (
    <nav className={styles.nav}>
      <Tabs value={currentPath}
            indicatorColor='primary'
            orientation={tabsOrientation}>
        {
          tabs.map(tab =>
            <Tab onClick={(): void => history.push(tab.path)}
                 classes={tabClasses}
                 label={tab.label}
                 value={tab.path}
                 key={tab.key} />
          )
        }
      </Tabs>
    </nav>
  );
};
