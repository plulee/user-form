import React, {ReactElement} from 'react';
import {Link} from 'react-router-dom';
import {RoutePaths} from '../router/route-paths';
import logo from '../../assets/logo.svg';
import styles from './Header.module.scss';

export const Header = (): ReactElement => (
  <header className={styles.header}>
    <div className={styles.contentWrapper}>
      <Link to={RoutePaths.ROOT} className={styles.link}>
        <img src={logo} className={styles.logo} alt="logo" />
        React User Form
      </Link>
    </div>
  </header>
);
