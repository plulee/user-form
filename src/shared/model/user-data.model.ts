export interface UserData {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  birthday?: string;
  about?: string;
  avatar?: string;
}
