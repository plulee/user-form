import {useEffect, useState} from 'react';
import MockedHttpService from '../mocked-http.service';
import {UserData} from '../model/user-data.model';

const emptyUserData = {
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
  birthday: '',
  about: '',
  avatar: ''
};

export default function useGetUserDataHook(): {isUserDataLoading: boolean, userData: UserData} {

  const [userData, setUserData] = useState<UserData>(emptyUserData);
  const [isUserDataLoading, setUserDataLoading] = useState<boolean>(true);

  useEffect(() => {
    MockedHttpService.getUserData()
      .then(userData => {
        if (userData) {
          setUserData(userData);
        }
        setUserDataLoading(false);
      })
  }, []);
  
  return {isUserDataLoading, userData};
}

