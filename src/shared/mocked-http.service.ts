import {UserData} from './model/user-data.model';

const MOCKED_API_TIMEOUT = 700;

class MockedHttpService {

  private userData?: UserData;

  getUserData = (): Promise<UserData | undefined> => new Promise(resolve => {
    setTimeout(() => {
      resolve(this.userData);
    }, MOCKED_API_TIMEOUT);
  });

  postUserData = (data: UserData): Promise<UserData> => new Promise(resolve => {
    setTimeout(() => {
      this.userData = data;
      resolve(this.userData);
    }, MOCKED_API_TIMEOUT);
  });
}

export default new MockedHttpService();